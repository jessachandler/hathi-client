## hathi-client

### Purpose
The hathi-client is intended for testing the hathi-server.

### Status
The current version of the client is a python client and server pair. The python server echoes whatever the python client sends. The python client will eventually be used to test the hathi-server. For now, it works in parallel with the python server.  

### Use
Using CLI

```
git clone `copy from hathi-client` hathi-client
cd hathi-client
python3 server.py
```
open another shell window

```
cd hathi-client
python3 client.py
```

Client and server both have rough argparse to explain the few choices available in the strawman versions
