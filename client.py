#!/usr/bin/env python
# # -*- coding: utf-8 -*-

"""client.py
created as strawman

connects to a server, sends a message, and repeats what the server returns
"""

import sys
import socket
import argparse


parser = argparse.ArgumentParser( description='A simple message client', epilog="run python3 server.py before running the client in another shell")
parser.add_argument('-host', default='127.0.0.1',
                    help='a specific host if not the localhost')
parser.add_argument('-message', type=str, default="message",
                    help='a specific port (default: message)')

args = parser.parse_args()

# get local machine - hardcode
serverHost = '127.0.0.1'          #socket.gethostname()
serverPort = 50008

if len(sys.argv) > 1:
    serverHost = sys.argv[1]  # if given host, use that
    if len(sys.argv) > 2:
        message = sys.argv[2]  # if given message, use that


sockobj = socket.socket(socket.AF_INET, socket.SOCK_STREAM) # create a socket object
sockobj.connect((serverHost, serverPort))

message = ['message']
for line in message:
    sockobj.send(line.encode())
    data = sockobj.recv(1024) # limit return size
    print('server responded: ', repr(data))

#print("The Server thinks it has time: %s" % tm.decode('ascii'))

sockobj.close() # ensure the connection closes

# End of File
